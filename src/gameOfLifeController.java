
import javafx.event.EventHandler;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class gameOfLifeController implements Runnable {

	
    @FXML
    private AnchorPane ancpane;

    @FXML
    private Label genlabel;

    @FXML
    private ComboBox<String> hastighetdropdown;

    @FXML
    private Button stepbtn;

    @FXML
    private Button startbtn;

    @FXML
    private Button pausebtn;

    @FXML
    private TextField radertxt;

    @FXML
    private TextField kolonnertxt;

    @FXML
    private Button resetbtn;
   
    @FXML
    private GridPane gridpane;

    @FXML
    private TilePane tilepane;
    
    /*egendefinerte variabler*/
    
    final long timeInterval = 1000;
    GameOfLife gameoflife; 
    Boolean active = false; //kj�r gameoflife 
    int gprow = 10; //size of grid
	int gpcoll= 10;
	double waittime = 2; //default generasjon hastighet 
	Pane pane;
	GridPane gp = new GridPane();
	int step = 0; //current generasjon 

    

    @FXML
    void pauseact(ActionEvent event) { 			//pause knapp funksjon 
    	
    	active = false; 						//pauser animasjonen
    	
    	stepbtn.setDisable(false); 				//reaktiverer step knappen 
    	gp.setDisable(false); 					//gir brukern tilgang til grid panet igjen
    	hastighetdropdown.setDisable(false);	//reakitverer drop down menyen
    	startbtn.setDisable(false); 			//reakitverer start knappen
    	resetbtn.setDisable(false);				//reaktiviterer reset


    }

    @FXML
    void resetact(ActionEvent event) { 		 //reset knapp funksjon
    	
    	genlabel.setText(String.valueOf(0)); //sett generasjon til 0
    	step=0; 							 //resetter generasjoner
    	gprow = Integer.valueOf(radertxt.getText()); //hent bruker input for grid st�rrelse
    	gpcoll= Integer.valueOf(kolonnertxt.getText());
    	gp.getChildren().clear();			 //clear alle gridpane barn for � ung� duplikate barn under initialize
    	ancpane.getChildren().remove(gp); 	 //drop gridpane fra ancorpanet 
    	initialize();						 //kj�r init p� nytt med nye verdier
    	
    }
    @FXML
    void startact(ActionEvent event) throws InterruptedException { //start knapp funksjon
    	active = true; 						//start generering 
    	startbtn.setDisable(true); 			//disabler start knappen
    	stepbtn.setDisable(true); 			//disabler step knappen 	
    	gp.setDisable(true);				//disabler gridpane for bruker input
    	hastighetdropdown.setDisable(true);	//disabler dropdown menyen
    	resetbtn.setDisable(true);			//disabler reset
    	//https://stackoverflow.com/questions/20497845/constantly-update-ui-in-java-fx-worker-thread
    	
    	Task<Void> task = new Task<Void>() {
    		  @Override
    		  public Void call() throws Exception {
    		    int i = 0; 
    		    while (active) { 			//kj�r s� lenge pause ikke er trykket p� 
    		      Platform.runLater(new Runnable() {
    		        @Override 
    		        public void run() {
    		        	genlabel.setText(String.valueOf(step));
    		        	step++;
    		        	gameoflife.nextgen(); 	//regn ut neste generasjon 
    		        	update();				//update grafikken 
    		        	
    		        }
    		      });
    		      i++;
    		      Thread.sleep((long) (waittime*1000)); //venter en viss tid basert p� bruker innput
    		    }
				return null;
    		  }
    		};
    		Thread th = new Thread(task);
    		th.setDaemon(true);
    		th.start();
    	
    }
    	


    @FXML
    void stepact(ActionEvent event) {			//generasjon teller
    	
    	genlabel.setText(String.valueOf(step)); //oppdaterer generasjons tellern
    	step++;
    	gameoflife.nextgen();					//regner ut neste gen
    	update();								//oppdaterer brett
    	
    }
    
    private void update() {						//update grafikken 
	    //kode basert p� https://stackoverflow.com/questions/20825935/javafx-get-node-by-row-and-column
	    
		ObservableList<Node> childrens = gp.getChildren(); //hent alle barn 
		
		for (Node node : childrens) { 						//for alle barn 
			
			if (GridPane.getRowIndex(node)==null ||GridPane.getColumnIndex(node)==null )
				continue; 									//om nullptr, hopp ut av loopen 
			
			if (gameoflife.board[GridPane.getColumnIndex(node)][GridPane.getRowIndex(node)]==0) {
        		node.setStyle("-fx-background-color: white;"); //om noden er d�d, gj�r den hvit

			}
			else
				node.setStyle("-fx-background-color: black;"); //ikke d�d = live, sett noden til svart.  
	     }
	    

	}
    

    @FXML
    void changetime(ActionEvent event) {	//combo box funksjonen
        
    	String s = hastighetdropdown.getValue();	
    	//henter bruker input, blir henta som string verdi 
    	//siden jeg ikke fant ut hvordan jeg henta hvilken skuff som var valgt
    	
    	switch (s) {
		case "0.5 generasjoner per sekund": 	//0.5 gens per sekund, vent 2 sek
			waittime = 2;
			break;
		case "1 generasjoner per sekund":		//1 gen per sek, vent 1 sek
			waittime = 1;
			break;
		case "2 generasjoner per sekund":		//kj�r oppdateringen 2 ganger i sekundet 
			waittime = 0.5;

			break;

		default:
			waittime = 0.5;						//burde ikke f� inn her, men grei � ha
			break;
		}
    	    	 
    }

	public void initialize()
    {	
    	gameoflife = new GameOfLife(gprow,gpcoll);		//lager nytt brett av xy st�rlese
    	
    	radertxt.setText(String.valueOf(gprow));		//setter tekst feltene til � v�re riktig verdi
    	kolonnertxt.setText(String.valueOf(gpcoll));
    	gprow = Integer.valueOf(radertxt.getText()); 	//largrer verdien unna 
    	gpcoll= Integer.valueOf(kolonnertxt.getText());
    	
    	hastighetdropdown.getItems().addAll(			//setter opp dropdown menyen
    		    "0.5 generasjoner per sekund",
    		    "1 generasjoner per sekund",
    		    "2 generasjoner per sekund"
    		);
    	
    	hastighetdropdown.getSelectionModel().selectFirst();  //setter dropdown menyens default valg
    	
    	
    	//formaterer gridpane 
    	gp.setGridLinesVisible(true);
    	
    	gp.setHalignment(ancpane, HPos.CENTER);
    	
    	//fyller griddet med tomme noder
    	/*basert op https://stackoverflow.com/questions/31095954/
    	/ how-to-get-gridpane-row-and-column-ids-on-mouse-entered-in-each-cell-of-grid-in*/
    	
    	
    	//setter hvite panes i alle nodene i gird panet
    	for(int i=0;i<gprow;i++){
            for(int j=0;j<gpcoll;j++){
                Pane pane = new Pane();
                pane.setStyle("-fx-background-color: white;");
                pane.setPrefSize(500/gprow,500/gpcoll); //om panelene var ekstremt store s� blir de 
                										//nedskalert til � passe i ancorpanet
                pane.autosize();
                pane.setOnMouseReleased(new EventHandler<MouseEvent> () {	//hver pane fanger opp mouseevents
                public void handle(MouseEvent me) {
                    	
                    	//finner hvem pane som ble trykket p�, setter fargen til � v�re det omvendte 
                		/* kode basert p� 
                		 * https://code.i-harness.com/en/q/2b1fed4
                		 */
                	
                        Node source = (Node)me.getSource() ;
                    	Integer colIndex = GridPane.getColumnIndex(source);
                        Integer rowIndex = GridPane.getRowIndex(source);
                        
                        if (gameoflife.flipCell(colIndex, rowIndex) == 0) //flipper cella og setter farge
                        {
                        	pane.setStyle("-fx-background-color: transparent;"); 
						}
                        else
                        {
                            pane.setStyle("-fx-background-color: black;");
                        }

                    }
                });

                gp.add(pane, i,j ); //legger til panet i gridpane sin i,j
                
            }
    	}
      
    	AnchorPane.setTopAnchor(gp, 0.0); //resize gridpanet til � passe til ancorpanet
    	AnchorPane.setRightAnchor(gp, 0.0);
    	AnchorPane.setLeftAnchor(gp, 0.0);
    	AnchorPane.setBottomAnchor(gp, 0.0);

    	//gp.autosize();    //formatering av gridpane
    	gp.setGridLinesVisible(true);
    	gp.setHgap(2); //setter mellomrom mellom panene
    	gp.setVgap(2);
    	
    	ancpane.getChildren().add(gp); //legger gridpane under ancorpanet 
    	ancpane.setVisible(true);
    
        
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
