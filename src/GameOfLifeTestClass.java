

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class GameOfLifeTestClass {
	
	/*
	 * jeg skal ha lagt alle oppgavene til sin egen funksjon, funksjonsnavnet skal likne p� 
	 * oppgave navnet fra #1 testene 
	 */
	
	
	@Test
	public void initTest() {

		
		/*
		 * tester her brettet
		 */
		GameOfLife gameoflife = new GameOfLife(4,8);
		
		assertEquals(4,gameoflife.getNumberOfRows());
		assertEquals(8,gameoflife.getNumberOfCollums());
	}
	
	/* tester her "Ingen levende naboer p� et tomt brett"*/
	@Test
	public void ingenNaboer() {
		GameOfLife gameoflife = new GameOfLife(4,8);
	
		assertEquals(0,gameoflife.getCellValue(0,0));

		assertEquals(0,gameoflife.getNumbOfNeighbours(1,4));
	}
	/* tester her "Finner en levende nabo"*/
	@Test
	public void levendeNabo() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		assertEquals(1,gameoflife.getCellValue(0,3));
		assertEquals(1,gameoflife.getNumbOfNeighbours(1,4));
		gameoflife.setCellAlive(2,3);
		assertEquals(1, gameoflife.getNumbOfNeighbours(3,3));
		
	}
	
	/* tester her "Finner to levende naboer" til "Finner �tte levende naboer"  */
	@Test
	public void toLevende() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		assertEquals(2,gameoflife.getNumbOfNeighbours(1,4));

	}
	@Test
	public void treLevende() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);

		assertEquals(3,gameoflife.getNumbOfNeighbours(1,4));

	}
	@Test
	public void fireLevende() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);
		gameoflife.setCellAlive(1,5);

		assertEquals(4,gameoflife.getNumbOfNeighbours(1,4));

	} 
	@Test
	public void femLevende() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);
		gameoflife.setCellAlive(1,5);
		gameoflife.setCellAlive(2,5);

		assertEquals(5,gameoflife.getNumbOfNeighbours(1,4));

	}
	@Test
	public void seksLevende() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);
		gameoflife.setCellAlive(1,5);
		gameoflife.setCellAlive(2,4);
		gameoflife.setCellAlive(2,5);

		assertEquals(6,gameoflife.getNumbOfNeighbours(1,4));

	}
	@Test
	public void syvLevende() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);
		gameoflife.setCellAlive(1,5);
		gameoflife.setCellAlive(2,4);
		gameoflife.setCellAlive(2,5);
		gameoflife.setCellAlive(2,3);

		assertEquals(7,gameoflife.getNumbOfNeighbours(1,4));

	}
	@Test
	public void atteLevende() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);
		gameoflife.setCellAlive(1,3);
		gameoflife.setCellAlive(1,5);
		gameoflife.setCellAlive(2,3);
		gameoflife.setCellAlive(2,4);
		gameoflife.setCellAlive(2,5);

		assertEquals(8,gameoflife.getNumbOfNeighbours(1,4));

	}
	
	/*hj�rne tester*/
	@Test
	public void venstreHjorne() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,1);
		gameoflife.setCellAlive(1,0);
		gameoflife.setCellAlive(1,1);


		assertEquals(3,gameoflife.getNumbOfNeighbours(0,0));

	}
	@Test
	public void hoyereHjorne() {

		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(3,6);
		gameoflife.setCellAlive(2,6);
		gameoflife.setCellAlive(2,7);


		assertEquals(3,gameoflife.getNumbOfNeighbours(3,7));

	}
	
	/*En celle med mindre enn to naboer d�r*/
	@Test
	public void mindreEnToNaboer() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,0);
		gameoflife.setCellAlive(0,1);
		assertEquals(1,gameoflife.getCellValue(0,0));

		gameoflife.nextgen();

		assertEquals(0,gameoflife.getCellValue(0,0));

	}
	/*En celle med minst to naboer lever*/
	@Test
	
	
	public void toNaboer() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,0);
		gameoflife.setCellAlive(0,1);
		gameoflife.setCellAlive(1,1);

		assertEquals(1,gameoflife.getCellValue(0,0));

		gameoflife.nextgen();

		assertEquals(1,gameoflife.getCellValue(0,0));

	}
	/*En levende celle med mer enn tre naboer d�r*/
	@Test
	public void flereEnToNaboer() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);
		gameoflife.setCellAlive(1,4);
		gameoflife.setCellAlive(1,5);
		assertEquals(1,gameoflife.getCellValue(1,4));

		gameoflife.nextgen();

		assertEquals(0,gameoflife.getCellValue(1,4));

	}
	/*En d�d celle med eksakt tre naboer blir levende*/
	@Test
	public void gjennoppsta() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(0,3);
		gameoflife.setCellAlive(0,4);
		gameoflife.setCellAlive(0,5);
	
		assertEquals(0,gameoflife.getCellValue(1,4));

		gameoflife.nextgen();

		assertEquals(1,gameoflife.getCellValue(1,4));

	}
	/*Test av et helt brett*/
	@Test
	public void heltBrettTest() {
		GameOfLife gameoflife = new GameOfLife(4,8);
		gameoflife.setCellAlive(1,4);
		gameoflife.setCellAlive(2,3);
		gameoflife.setCellAlive(2,4);
		
		assertEquals(0,gameoflife.getCellValue(1,3));
		assertEquals(1,gameoflife.getCellValue(1,4));
		assertEquals(1,gameoflife.getCellValue(2,3));
		assertEquals(1,gameoflife.getCellValue(2,4));
		
		//antar at oppgaven mener etter neste generasjon
		gameoflife.nextgen();

		assertEquals(1,gameoflife.getCellValue(1,3));
		assertEquals(1,gameoflife.getCellValue(1,4));
		assertEquals(1,gameoflife.getCellValue(2,3));
		assertEquals(1,gameoflife.getCellValue(2,4));

	}
}
