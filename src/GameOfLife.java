


public class GameOfLife {

	
	//getter for antall rader
	int gameoflifeRow;
	public int getGameoflifeRow() {
		return gameoflifeRow;
	}
 
	//getter for antall kolonner
	int gameoflifeCollum;
	public int getgameoflifeCollum() {
		return gameoflifeCollum;
	}
	
	//brett er dobbel array
	int board[][]; 
	
	//konstruktor som tar inn 2 ints, looper over og setter hele brettet til d�dt
	GameOfLife(int row, int collum)
	{
		gameoflifeRow = row; 
		gameoflifeCollum = collum;
		board = new int[row][collum] ;

		//setter opp et tomt brett 
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < collum; j++) {
				board[i][j]=0;
			}
		}
	}
	
	
	Object getNumberOfCollums() {
		// TODO Auto-generated method stub
		return gameoflifeCollum;
	}


	Object getNumberOfRows() {
		// TODO Auto-generated method stub
		return gameoflifeRow;
	}

	//sjekker statusen til en bestemt celle, 0 = d�d, 1= live,
	public Object getCellValue(int i, int j) {
		int value = board[i][j];
		return value;
	}


	//looper rundt en bestemt celle � teller antall naboer
	public Object getNumbOfNeighbours(int row, int coll) {
		
		int naboAntall = 0; 
		//System.out.println(getGameoflifeRow());
		//System.out.println(getgameoflifeCollum());
		int maxrow = getGameoflifeRow()-1;
		int maxcoll = getgameoflifeCollum()-1;
	
		
		//basert p� koden her https://www.dreamincode.net/forums/topic/325812-neighbour-cell-detection-in-2d-array/
		for (int i = -1; i <=1; i++) {
			for (int j = -1; j<= 1; j++) {
				if (i == 0 && j ==0) {
					continue; //if self, skip ut av loopen
				}
				
				if ((row+i <0) || row+i>maxrow)
					continue; //if out of bounds, skip ut av loopen
				if ((coll+j <0) || coll+j>maxcoll)
					continue; //if out of bounds, skip ut av loopen
				if (board[row+i][ coll+j]   != 0) 
					naboAntall++; //legger til en p� antall totale naboer
				
				}
			}
		
		return naboAntall;
	}


	//setter en celle til ilive
	public void setCellAlive(int i, int j) {
		
		board[i][j]=1;
	}
	//dreper en celle
	public void setCellDead(int i, int j) {
		
		board[i][j]=0;
	}
	//flipper en celles verdi
	public int flipCell(int i, int j)
	{
		if (board[i][j] == 0)
			board[i][j]=1;
		else 
			board[i][j]=0;
		
		return board[i][j];
	}
	
	//kalkulerer neste generasjon
	public void nextgen() { 
		
		int maxrow = getGameoflifeRow();
		int maxcoll = getgameoflifeCollum();	
		
		int [][] boardcopy = new int[board.length][];
		for(int i = 0; i < board.length; i++)
			boardcopy[i] = board[i].clone();
		//based on this code https://stackoverflow.com/questions/5617016/
		//how-do-i-copy-a-2-dimensional-array-in-java/13792183
		
		//oppretter en kopi av brettet som vi kan jobbe med

		for (int i = 0; i < maxrow; i++) {
			for (int j = 0; j <maxcoll; j++) {
				int nabo = (int) getNumbOfNeighbours(i,j);
				//System.out.println(nabo);
				if (nabo <2) {
					boardcopy[i][j] = 0; //mindre en 2 dreper den
				}
				if (nabo >3) {
					boardcopy[i][j] = 0; //flere en 3 dreper 
				}
				if (nabo == 3)
				{
					boardcopy[i][j] = 1; //akkurat 3 gjennoppliver, trenger ikke sjekke om den er d�d eller ikke
				}
			}
		}
		

		board = boardcopy; //setter det origniale brettet til � v�re lik kopien

	}


	public int[][] getGrid() {
		
		// TODO Auto-generated method stub
		return board;
	}
	
		
}
